import React from "react";
import './App.css';
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";

class App extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            firstModal : false,
            secondModal : false,
        }
    }
    openModal(){
        this.setState((state) => ({
            ...state, firstModal : !state.firstModal
        }))
        console.log("click");
    }
    render() {
        return(
            <div className="App">
                <Modal isOpen={this.state.firstModal} header={"First modal"} closeButton={"X"}/>
                <Button text={"Open first modal"} style={{backgroundColor: "black"}} onClick={() => this.openModal()}/>
                <Button text={"Open second modal"}/>
            </div>
        );
    }
}

export default App;
