import React, {Component} from 'react';

class Button extends Component {
    constructor({text, backgroundColor, onClick, style}) {
        super();
        this.text = text;
        this.backgroundColor = backgroundColor;
        this.onClick = onClick;
        this.style = style;
    }
    render() {
        return (
            <button style = {this.style} onClick={this.onClick}>{this.text}</button>
        );
    }
}

export default Button;