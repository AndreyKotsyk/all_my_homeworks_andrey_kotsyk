// при работе с input лучше не использовать события клавиатуры потому, что
// у данного тега есть свои методы которые можно использовать.
const buttonWrapper = document.getElementsByClassName("btn-wrapper");
let buttons = document.getElementsByClassName("btn");

document.body.addEventListener("keydown", e => {
    let pressedKey = e.key.toLowerCase();
    buttons = Array.from(buttons);
    buttons.forEach(btn => btn.classList.remove("active"));
    for(let i=0; i<buttons.length; i++){
        if(pressedKey === buttons[i].textContent.toLowerCase()){
            buttons[i].classList.add("active");
        }
    }
});

