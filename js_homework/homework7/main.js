// Система в которой каждый html елемент это объект c которым можно
// взаимодействовать(добавлять/удалять елементы, присваивать стили и тд
function createList(array, parent = document.body) {
    let createUl = document.createElement("ul");
    parent.append(createUl);
    let result = array.map(function(element) {
        return `<li>${element}</li>`
    });
    createUl.innerHTML = result.join('');
}