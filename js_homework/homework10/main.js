const password = document.getElementsByClassName("password");
const form = document.getElementsByTagName("form")[0];
const submit = document.getElementsByClassName("btn")[0];

form.addEventListener("click", e => {
    if(e.target.classList.contains("fa-eye")){
        e.target.classList.replace("fa-eye", "fa-eye-slash");
        e.target.previousElementSibling.type = "password";
    }else if(e.target.classList.contains("fa-eye-slash")){
        e.target.classList.replace("fa-eye-slash", "fa-eye");
        e.target.previousElementSibling.type = "text";
    }
});
submit.addEventListener("click", e => {
    e.preventDefault();
    if(password[0].value === password[1].value){
        alert("You are welcome");
    } else {
        alert("Нужно ввести одинаковые значения");
    }
    password[0].value = "";
    password[1].value = "";
});
