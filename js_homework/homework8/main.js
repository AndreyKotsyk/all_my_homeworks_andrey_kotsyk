// Обработчик событий это фцнкция которая срабатывает при любом событии
// в обьекте и помогает реагировать на действия пользователя
const input = document.querySelector(".input");
const span = document.createElement("span");
const closeSpan = document.createElement("button");

input.addEventListener("focus", () => {
    input.classList.add("focused");
});
input.addEventListener("blur", () => {
    input.classList.remove("focused");
    let price = input.value;

    span.textContent = `Текущая цена: ${price}`;
    document.body.append(span);

    closeSpan.textContent = "x";
    document.body.append(closeSpan);
    input.style.background = "green";

    if(price <= 0){
        input.style.background = "red";
        alert("Please enter correct price"); // надо вывести под полем не создавая span
    }
});
closeSpan.addEventListener("click", () => {
    span.remove();
    closeSpan.remove();
    input.value = "";
    input.style.background = "";
});
