//1) setTimeout() выполняет тело функции 1 раз после задержки, setInterval() выполняет тело функции пока не будет прописан clearInterval()
//так же setTimeout имеет фиксированую задержку после выполнения callback функции
//2) если задать нулевую задержку, то планировщик заданий планирует выполнить функцию как можно быстрее после текужего кода
//3)clearInterval стоит вызывать когда цикл уже не нужен потому, что большое количество таймеров могут очень сильно перегрузить страницу

let timerId;
function slider(){
    timerId = setInterval(() => {
        gallery[tabId].classList.add("hidden");
        tabId ++;
        if(tabId < gallery.length){
            gallery[tabId].classList.remove("hidden");
        }else{
            tabId = 0;
        }
    }, 2000);
}

let gallery = document.getElementsByClassName("image-to-show");
gallery = Array.from(gallery);
gallery.forEach(img => img.classList.add("hidden"));
let firstImg = gallery[0];
firstImg.classList.remove("hidden");
let tabId = firstImg.dataset.id;

slider();

const btnStop = document.querySelector(".stop");
const btnStart = document.querySelector(".start");

btnStop.addEventListener("click", e => {
    clearInterval(timerId);
});

btnStart.addEventListener("click", e => {
    slider();
});