let titles = document.querySelectorAll(".tabs-title");
let tabs = document.querySelectorAll(".tab-items");
const titlesParent = document.querySelector(".tabs");

titles[0].classList.add("active");
tabs[0].classList.add("active"); // изначально активный 1 title 1 tab

titlesParent.addEventListener("click", e => {
    titles = Array.from(titles);
    tabs = Array.from(tabs);

    let tabId = e.target.dataset.id;
    titles.forEach(title => title.classList.remove('active'));
    let targetTitle = titles.find(tab => tab.dataset.id === tabId);
    targetTitle.classList.add('active');

    tabs.forEach(block => block.classList.remove('active'));
    let targetTab = tabs.find(block => tabId === block.dataset.id);
    targetTab.classList.add('active');
});

