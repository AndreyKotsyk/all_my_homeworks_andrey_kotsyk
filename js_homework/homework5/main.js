// экранирование это способ передачи символов которые "понимает" интерпритатор, определенными спецсимволами
// для того, что бы не было синтаксической ошибки при написании кода
const User = function () {
    const firstName = prompt("Enter name",'Andrey');
    const lastName = prompt("Enter last name", 'Kotsyk');
    const birthday = prompt("When you have born? day.month.year", '15.03.2003');
    const now = new Date();
    this.name = firstName;
    this.lastname = lastName;
    this.birthday = birthday;
    this.getLogin = () => {
        this.initials = firstName[0].toLowerCase() + lastName.toLowerCase();
    }
    this.getPassword = () => {
        this.password = firstName[0].toUpperCase() + lastName.toLowerCase() + birthday.slice(6,10);
    }
    this.getAge = () => {

    }
};
const createUser = new User();
createUser.getLogin();
createUser.getPassword();
console.log(createUser);
