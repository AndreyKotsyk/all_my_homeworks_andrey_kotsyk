// 1) функции нужны для того, что бы разбивать задачу на подзадачи и избежать дублирования кода
// 2) аргументы передаются в функцию для того, что бы при вызове функции указать для каких значений выполнить
// набор команд в функции
function calculate(a, b, oper){
    switch (oper){
        case "*":
            return a*b;
        case "/":
            return a/b;
        case "+":
            return a+b;
        case "-":
            return a-b;
        default:
            alert("Incorrect operation")
    }
}

let n1 = prompt("Enter first number");
let n2 = prompt("Enter second number");
let operation = prompt("Enter operation(+,-,*,/)");

console.log(calculate(n1, n2, operation));

