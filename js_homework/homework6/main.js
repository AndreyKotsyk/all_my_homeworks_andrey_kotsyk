// forEach перебирает каждый елемент массива и вызывает для него callback, не создавая копии и не меняя исходный массив

const filterBy = (array, type) => {
    let res = array.filter(element => {
            if(element === null && type === null){
                return true;
            }else if(element !== null && typeof(element) === type){
                return true;
            }
        });
    return res;
}