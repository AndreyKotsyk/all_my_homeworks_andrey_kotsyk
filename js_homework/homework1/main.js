// 1)let переменная видна только в определенном блоке, а var глобальная. let видно только после объявления.
// Удобнее в цикле но нельзя переобъявлять. const тоже что и переменная let которую нельзя изменить после объявления
//2)var старел потому, что let и const гораздо удобнее и они имеют блочную видимость(главный плюс)


let name = prompt('What is your name?','Andrey');
let age = prompt('How old are you?', 18);

if (age < 18) {
    alert("You are not allowed to visit this website.");
} else if(age > 22){
    alert(`Welcome, ${name}`)
}else{
    confirm('Are you sure you want to continue?');
    alert(`Welcome, ${name}`);
}