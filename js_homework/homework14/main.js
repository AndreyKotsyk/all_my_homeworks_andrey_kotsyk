$(".btn-toggle-top-rated").on("click", function () {
    $(".top-rated-hotels-gallery").slideToggle("slow");
});

$(window).on("scroll", function (){
    if( $(window).scrollTop() > $(window).innerHeight() ){
        $("#scroll-top").show();
    }else{
        $("#scroll-top").hide();
    }
});