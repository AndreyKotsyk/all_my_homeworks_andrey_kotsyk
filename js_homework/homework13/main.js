document.addEventListener("DOMContentLoaded", getThemeFromStorage);
const body = document.getElementsByTagName("body")[0];

function toggleTheme(){
    if(body.classList.contains("dark-theme")){
        body.classList.remove("dark-theme");
        localStorage.setItem("theme", "");
    }else{
        localStorage.setItem("theme", "dark-theme");
        body.classList.add("dark-theme");
    }
}

function getThemeFromStorage(){
    if(localStorage.getItem("theme")){
        const theme = localStorage.getItem("theme");
        body.classList.add(theme);
    }
}

const button = document.getElementById("change-theme");
button.addEventListener("click", toggleTheme);