// асинхронность в js это когда код выполняется паралельно синхронному коду(асинхронный код обычно заворачивают в promise
// что бы выполнять его через then, потому что нельзя заранее знать когда асинхронный код закончит выполнение)
const findByIP = async () => {
    const res = await fetch("https://api.ipify.org/?format=json");
    const data = await res.json();
    const {ip} = data;

    const response = await fetch(`http://ip-api.com/json/${ip}`);
    const address = await response.json();
    const addressInfo = document.getElementById("address-info");
    const {continent, country, regionName, city, district} = address;
    addressInfo.insertAdjacentHTML("afterbegin", `
        <li>${continent}</li>
        <li>${country}</li>
        <li>${regionName}</li>
        <li>${city}</li>
        <li>${district}</li>`)
}
const btn = document.querySelector(".btn-find-ip");
btn.addEventListener("click", findByIP);