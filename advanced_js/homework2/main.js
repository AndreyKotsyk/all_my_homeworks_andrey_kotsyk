// try catch надо использовать когда ожидается какая-то ошибка(не валидные данные пришли в форме, отсутствие метода или свойства в объекте)
// #1
function createList(array) {
    const parent = document.getElementById("root");
    let createUl = document.createElement("ul");
    parent.append(createUl);
    let result = array.map(function(element){
        try{
            if(element.author&&element.name&&element.price){
                return `<li>${element.author}</li><li>${element.name}</li><li>${element.price}</li>`
            }else {
                throw Error;
            }
        }catch (err){
            if(!element.author){
                console.log("no author");
            }else if(!element.name){
                console.log("no name");
            }else if(!element.price){
                console.log("no price");
            }
        }
    });
    createUl.innerHTML = result.join('');
}

const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

createList(books);