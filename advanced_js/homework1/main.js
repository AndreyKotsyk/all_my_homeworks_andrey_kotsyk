// деструктуризация это разбивка чего-то масштабного (объекта или массива) на части по-меньше
// №1
const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];
let allClients = [...clients1 , ...clients2];

uniq = [...new Set(allClients)];
console.log(uniq);

// №2
const characters = [
    {
        name: "Елена",
        lastName: "Гилберт",
        age: 17,
        gender: "woman",
        status: "human"
    },
    {
        name: "Кэролайн",
        lastName: "Форбс",
        age: 17,
        gender: "woman",
        status: "human"
    },
    {
        name: "Аларик",
        lastName: "Зальцман",
        age: 31,
        gender: "man",
        status: "human"
    },
    {
        name: "Дэймон",
        lastName: "Сальваторе",
        age: 156,
        gender: "man",
        status: "vampire"
    },
    {
        name: "Ребекка",
        lastName: "Майклсон",
        age: 1089,
        gender: "woman",
        status: "vempire"
    },
    {
        name: "Клаус",
        lastName: "Майклсон",
        age: 1093,
        gender: "man",
        status: "vampire"
    }
];
let charactersShortInfo = [];
for(let i=0; i < characters.length; i++){
    let {name, lastName, age} = characters[i];
    let tempObj = {name, lastName, age};
    charactersShortInfo.push(tempObj);
}
console.log(charactersShortInfo);

// №3

const user1 = {
    name: "John",
    years: 30
};

let {name, years, isAdmin = false} = user1;
console.log(name);
console.log(years);
console.log(isAdmin);

// №4

const satoshi2020 = {
    name: 'Nick',
    surname: 'Sabo',
    age: 51,
    country: 'Japan',
    birth: '1979-08-21',
    location: {
        lat: 38.869422,
        lng: 139.876632
    }
}

const satoshi2019 = {
    name: 'Dorian',
    surname: 'Nakamoto',
    age: 44,
    hidden: true,
    country: 'USA',
    wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
    browser: 'Chrome'
}

const satoshi2018 = {
    name: 'Satoshi',
    surname: 'Nakamoto',
    technology: 'Bitcoin',
    country: 'Japan',
    browser: 'Tor',
    birth: '1975-04-05'
}

let fullProfile = {...satoshi2018, ...satoshi2019, ...satoshi2020};
console.log(fullProfile);

// №5

const books = [{
    name: 'Harry Potter',
    author: 'J.K. Rowling'
}, {
    name: 'Lord of the rings',
    author: 'J.R.R. Tolkien'
}, {
    name: 'The witcher',
    author: 'Andrzej Sapkowski'
}];

const bookToAdd = {
    name: 'Game of thrones',
    author: 'George R. R. Martin'
}

let allBooks = [...books];
allBooks.push(bookToAdd);
console.log(allBooks);

// №6

const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
}
let age = 25;
let sallary = 3000;
let worker = {...employee, age, sallary};
console.log(worker);

// №7

const array = ['value', () => 'showValue'];
let [value, showValue] = array;
alert(value);
alert(showValue());
