//прототипное наследование можно использовать что бы создать объект(ы) на основе другого объекта
class Employee{
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    get getName(){
        return this._name
    }
    set setName(newName){
        this._name = newName;
    }
    get getAge(){
        return this._age
    }
    set setAge(newAge){
        this._age = newAge;
    }
    get getSalary(){
        return this._salary;
    }
    set setSalary(newSalary){
        this._salary = newSalary;
    }
}
class Programmer extends Employee{
    constructor(name, lang) {
        super(name);
        this._lang = lang;
    }
    get getSalary() {
        return this._salary*3;
    }
    set setSalary(newSalary) {
        this._salary = newSalary;
    }
    get getLang(){
        return this._lang;
    }
    set setLang(newLang){
        this._lang = newLang;
    }
}


let bob = new Programmer("Bob", 4);
bob.setAge = 23;
bob.setSalary = 25000;
console.log(bob);

let kate = new Programmer("Kate", 2);
kate.setAge = 24;
kate.setSalary = 20000;
console.log(kate)