// AJAX - это технология которая взаимодействует с сервером без перезагрузки страницы
fetch("https://ajax.test-danit.com/api/swapi/films")
    .then(response => response.json())
    .then(data => data.forEach(film => {
        const {episodeId, name, openingCrawl} = film;
        let filmInfo = document.createElement("div");
        document.body.prepend(filmInfo);
        filmInfo.insertAdjacentHTML("beforebegin", `
            <p>${name}</p>
            <p>${episodeId}</p>
            <p>${openingCrawl}</p>`);
        let characters = film.characters;
        characters.forEach(link => {
            fetch(link)
                .then(res => res.json())
                .then(character => {
                    let char = document.createElement("ul");
                    filmInfo.prepend(char);
                    char.insertAdjacentHTML("beforebegin", `
                        <li>${character.name}</li>
                        <li>${character.height}</li>
                        <li>${character.mass}</li>`)
                })
        })
    }))